# README

A docker container for running SweetHome3D.

## Start SweetHome3D
- normal startup: `docker-compose up sh3d`


## Known Issues
- the java GUI blacks out sections of the app from time to time - if you have or know of a fix please submit a PR
